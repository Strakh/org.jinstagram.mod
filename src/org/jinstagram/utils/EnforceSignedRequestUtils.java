package org.jinstagram.utils;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.jinstagram.auth.model.OAuthConstants;
import org.jinstagram.exceptions.InstagramException;

public class EnforceSignedRequestUtils {

	private static final String HMAC_SHA256 = "HmacSHA256";

	private static final String keyValuePairSpearator = "|";

	private static final String keyValueSpearator = "=";

	public static final String ENFORCE_SIGNED_REQUEST = "sig";

	private Mac mac;

	public EnforceSignedRequestUtils(String clientSecret)
			throws InstagramException {
		SecretKeySpec keySpec = new SecretKeySpec(clientSecret.getBytes(Charset
				.forName("UTF-8")), HMAC_SHA256);
		try {
			mac = Mac.getInstance(HMAC_SHA256);
			mac.init(keySpec);
		} catch (NoSuchAlgorithmException e) {
			throw new InstagramException("Invalid algorithm name!", e);
		} catch (InvalidKeyException e) {
			throw new InstagramException("Invalid key: " + clientSecret, e);
		}
	}

	public String signature(String methodName, Map<String, String> params,
			String accessToken) throws InstagramException {
		String toSignMsg = methodName;
		Map<String, String> sortedMap = new TreeMap<String, String>();
		sortedMap.put(OAuthConstants.ACCESS_TOKEN, accessToken);
		if (params != null && params.size() > 0)
			sortedMap.putAll(params);
		for (Entry<String, String> paramEntry : sortedMap.entrySet()) {
			toSignMsg += keyValuePairSpearator + paramEntry.getKey()
					+ keyValueSpearator + paramEntry.getValue();
		}
		byte[] result = mac
				.doFinal(toSignMsg.getBytes(Charset.forName("UTF-8")));
		String encodedResult = Hex.encodeHexString(result);
		return encodedResult;

	}
}
